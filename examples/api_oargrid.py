import logging
import os

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

NAME = "pyg5k"

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

sites = gk.sites.list()
site = gk.sites["rennes"]
sites = [gk.sites["rennes"], gk.sites["nancy"], gk.sites["grenoble"]]

# creates some jobs
jobs = []
for site in sites:
    job = site.jobs.create({"name": "pyg5k",
                            "command": "sleep 3600"})
    jobs.append(job)

_jobs = []
for site in sites:
    _jobs.append((site.uid, site.jobs.list(name=NAME,
                                           state="waiting,launching,running")))

print("We found %s" % _jobs)

# deleting the jobs
for job in jobs:
    job.delete()
