import logging

from grid5000 import Grid5000

logging.basicConfig(level=logging.DEBUG)

gk = Grid5000(
   uri="https://api-ext.grid5000.fr/stable/",
   sslcert="/path/to/ssl/certfile.cert",
   sslkey="/path/to/ssl/keyfile.key"
   )

gk.sites.list()

job = site.jobs.create({"name": "pyg5k",
                        "command": "sleep 3600"},
                        g5k_user = "auser1")


# Since the 'anonymous' user can not inspect jobs the following call will raise exception
# python-grid5000.exceptions.Grid5000AuthenticationError: 401 Unauthorized
job.refresh()

# Both following call work since any user can request info on any jobs.
job.refresh(g5k_user='auser1')
job.refresh(g5k_user='auser2')

# Some operations can only be performed by the jobs creator.
# The following call will raise exception
# pyg5k.exceptions.Grid5000DeleteError: 403 Unauthorized
job.delete(g5k_user='auser2')

# This call works as expected
job.delete(g5k_user='auser1')
