import logging
import json
from pathlib import Path
import os

from grid5000 import Grid5000, Grid5000Offline


logging.basicConfig(level=logging.DEBUG)


# First get a copy of the reference api
# This is a one time and out-of-band process, 
# here we get it by issuing a regular HTTP request
conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)
data = gk.dump_ref_api()

Path("ref.yaml").write_text(json.dumps(data))

# you can dump the data to a file
# and reuse it offline using the dedicated client
# here we reuse directly the data we got (no more HTTP requests will be issued)

ref = Grid5000Offline(json.loads(Path("ref.yaml").read_text()))
print(ref.sites["rennes"].clusters["paravance"].nodes["paravance-1"])
