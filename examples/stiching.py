conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

# List current stitchings
gk.stitcher.list()

# Sitching global vlan 16 to external vlan 1290
gk.stitcher.create({"id":"16", "sdx_vlan_id":"1290"})

# Get a a stitchings information by Grid'5000 global vlan id.
stitching = gk.stitcher.get('16')
# Or
stitching = gk.stitcher['16']

# End stitching
stitching = gk.stitcher.get('16')
stitching.delete()
# Or
gk.stitcher.delete('16')
