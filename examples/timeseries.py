import logging
import os

from grid5000 import Grid5000

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import time

logging.basicConfig(level=logging.INFO)

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

metrics = gk.sites["lyon"].clusters["nova"].metrics
print("--- available metrics")
print(metrics)

print("----- a timeserie")
now = time.time()
# NOTE that you can pass a job_id here
kwargs = {
    "nodes": "nova-1,nova-2,nova-3",
    "metrics": "wattmetre_power_watt",
    "start_time": int(now - 600),
}
metrics = gk.sites["lyon"].metrics.list(**kwargs)

# let's visualize this
df = pd.DataFrame()
for metric in metrics:
    timestamp = metric.timestamp
    value = metric.value
    device_id = metric.device_id
    df = pd.concat([df, pd.DataFrame({
        "timestamp": [timestamp],
        "value": [value],
        "device_id": [device_id]
    })])
sns.relplot(data=df,
            x="timestamp",
            y="value",
            hue="device_id",
            kind="line")
plt.show()
