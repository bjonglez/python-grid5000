import logging
import os

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

root_versions = gk.root.get().versions.list()
print(root_versions)

rennes = gk.sites["rennes"]
site_versions = rennes.versions.list()
print(site_versions)

cluster = rennes.clusters["paravance"]
cluster_versions = cluster.versions.list()
print(cluster_versions)

node_versions = cluster.nodes["paravance-1"]
print(node_versions)
