import logging
import os
import time

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)

conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)
site = gk.sites["rennes"]

job = site.jobs.create({"name": "pyg5k",
                        "command": "sleep 3600",
                        "resources": "{type='kavlan'}/vlan=1+nodes=1",
                        "types": ["deploy"]})

while job.state != "running":
    job.refresh()
    print("Waiting the job [%s] to be runnning" % job.uid)
    time.sleep(5)

deployment = site.deployments.create({"nodes": job.assigned_nodes,
                                      "environment": "debian9-x64-min",
                                      "vlan": job.resources_by_type["vlans"][0]})

while deployment.status != "terminated":
    deployment.refresh()
    print("Waiting for the deployment [%s] to be finished" % deployment.uid)
    time.sleep(10)

print(deployment.result)
