import logging
import os
import time

from grid5000 import Grid5000


logging.basicConfig(level=logging.DEBUG)


def _to_network_address(host, interface):
    """Translate a host to a network address
    e.g:
    paranoia-20.rennes.grid5000.fr -> paranoia-20-eth2.rennes.grid5000.fr
    """
    splitted = host.split('.')
    splitted[0] = splitted[0] + "-" + interface

    return ".".join(splitted)


conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)

site = gk.sites["rennes"]

job = site.jobs.create({"name": "pyg5k",
                        "command": "sleep 3600",
                        "resources": "{type='kavlan'}/vlan=1+{cluster='paranoia'}nodes=1",
                        "types": ["deploy"]
})

while job.state != "running":
    job.refresh()
    print("Waiting the job [%s] to be runnning" % job.uid)
    time.sleep(5)

vlanid = job.resources_by_type["vlans"][0]

# we hard code the interface but this can be discovered in the node info
# TODO: write the code here to discover
nodes = [_to_network_address(n, "eth2") for n in job.assigned_nodes]
print(nodes)

# set in vlan
site.vlans[vlanid].nodes.submit(nodes)
