import os

from grid5000 import Grid5000


conf_file = os.path.join(os.environ.get("HOME"), ".python-grid5000.yaml")
gk = Grid5000.from_yaml(conf_file)
site = gk.sites["rennes"]

#Get list of users using vlans
users = site.vlansusers.list()

#Get list of vlans a user is using.
user = site.vlansusers['msimonin']
print(user.vlans)

#Get list of users using a specific vlan
users = site.vlans['4'].users.list()

#Check if a user has access to a specific vlan.
user = site.vlans['4'].users['msimonin']
print(user.status)
